def greatest_common_factor(a, b):
    """
    :return: greatest common factor of a and b using Euler algorithm
    :type b: int
    :type a: int
    :rtype: int
    """
    if b == 0:
        return a
    return greatest_common_factor(b, a % b)


def multiplicative_order(A, N):
    """
    Refer about multiplicative order in https://en.wikipedia.org/wiki/Multiplicative_order
    :param A:
    :param N:
    :return: the multiplicative order of A modulo N
    :rtype: int
    """
    if greatest_common_factor(A, N) != 1:
        return -1
    result = 1

    k = 1
    while k < N:
        result = (result * A) % N
        if result == 1:
            return k
        k = k + 1

    return -1
