import Parasitic

if __name__ == '__main__':
    item = list()
    for right_most_digit in range(1, 10):
        for factor in range(1, 10):
            obj = Parasitic.ParasiticNumber(right_most_digit, factor)
            value = obj.value
            if value[0] != '0':
                item.append(obj)
                print(right_most_digit, factor, obj)
            else:
                print(right_most_digit, factor, 'not fulfill zero leading')
