import unittest
import Parasitic
import MultiplicativeOrder


class TestParasitic(unittest.TestCase):
    def test_main_method(self):
        input_ = ((7, 4), (6, 4), (2, 2))
        output = ('179487', '153846', '105263157894736842')
        for i in range(len(input_)):
            obj = Parasitic.ParasiticNumber(*input_[i])
            self.assertEqual(obj.value, output[i])


class TestMultiplicativeOrder(unittest.TestCase):
    def test_order(self):
        input_ = ((4, 7), (10, 39), (2, 19))
        output = (3, 6, 18)
        for i in range(len(input_)):
            abc = MultiplicativeOrder.multiplicative_order(*input_[i])
            self.assertEqual(abc, output[i])


if __name__ == '__main__':
    unittest.main()
