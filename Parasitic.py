import math
import MultiplicativeOrder as MultiOrder


class ParasiticNumber(object):
    right_most_digit: int
    multiply_factor: int
    length_repeating_decimal: int
    value: str

    def __init__(self, right_most_digit, multiply_factor):
        assert 0 < right_most_digit < 10
        assert 0 < multiply_factor < 10
        self.right_most_digit = right_most_digit
        self.multiply_factor = multiply_factor
        self.length_repeating_decimal = 0
        self.value = str()
        self.is_success = False
        self.execute()

    def execute(self):
        """
        Main function to get everything of object
        """
        multiplication_order = MultiOrder.multiplicative_order(10, 10 * self.multiply_factor - 1)
        if multiplication_order > 0:
            self.length_repeating_decimal = multiplication_order
            self.value = self.__get_specific_number_division(self.right_most_digit,
                                                             10 * self.multiply_factor - 1,
                                                             multiplication_order)
            self.is_success = True

    @staticmethod
    def __get_specific_number_division(dividend, divisor, number):
        """
        :type dividend: int
        :type divisor: int
        :type number: int
        :rtype: str
        :param number: number of elements should take
        """
        assert divisor > 0
        assert number > 0
        return_value = []
        count = 1
        while count <= number:
            count += 1
            temp_dividend = 10 * dividend
            return_value.append(str(temp_dividend // divisor))
            dividend = temp_dividend % divisor
        return ''.join(return_value)

    @staticmethod
    def is_parasitic_number(num):
        """
        Check if the number is parasitic number or not
        :return: number is parasitic number or not
        :type num: int
        :rtype: bool
        """
        if num < 10:
            return True
        last_digit = num % 10
        n = last_digit * (10 ** int(math.log10(num))) + (num // 10)
        return n % num == 0

    @staticmethod
    def is_parasitic_number_str(num):
        """
        Check if the number is parasitic number or not
        :return: number is parasitic number or not
        :type num: int
        :rtype: bool
        """
        if num < 10:
            return True
        num_str = str(num)
        num_invert = int(f'{num_str[-1]}{num_str[:-1]}')
        return num_invert % num == 0

    def __str__(self):
        if self.is_success:
            return f'Successful len={self.length_repeating_decimal} value={self.value}'
        else:
            return f'Fail'


if __name__ == '__main__':
    def test():  # Compare performance of 2 check method
        for num in range(10 ** 6):
            ParasiticNumber.is_parasitic_number(num)
            ParasiticNumber.is_parasitic_number_str(num)


    import cProfile

    cProfile.run('test()')
